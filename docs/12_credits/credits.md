---
author: Mireille Coilhac
title: 👏 Crédits
---

Le site est hébergé par  [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/) et [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/).

😀 Un grand merci à Vincent-Xavier Jumel pour la partie technique de ce site.   
Merci également à Charles Poulmaire pour ses relectures attentives et ses conseils judicieux.

Le logo :material-key-chain: fait partie de MkDocs sous la référence `key-chain`  [https://pictogrammers.com/library/mdi/](https://pictogrammers.com/library/mdi/){:target="_blank" }

